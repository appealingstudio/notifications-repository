<?php

namespace AppealingStudio\NotificationsRepository;

use Illuminate\Container\Container;

class NotificationsRepository
{
	/**
	 * Public variables
	 */
	public $notificate;

	/**
	 * Class variables
	 */
	protected $app;

	// --------------------------------------------------------------------

	/**
	 * Build a new WePay instace.
	 */
	public function __construct(Container $app)
	{
		$this->app = $app;
		$this->notificate = $this->app['notification'];
	}

	// --------------------------------------------------------------------

	public function getNotificationsArray($groupList = NULL)
	{
		$notificationGroups = array('error', 'warning', 'info', 'success');

		// Remove non desired groups
		if (!empty($groupList))
		{
			foreach ($notificationGroups as $key => $value)
			{
				if (!in_array($key, $groupList))
				{
					unset($notificationGroups[$key]);
				}
			}
		}

		// Get notification array by group
		foreach ($notificationGroups as $group)
		{
			$notificationsArray[$group] = $this->app['notification']->group($group)->showAll();
		}

		return $notificationsArray;
	}
}
